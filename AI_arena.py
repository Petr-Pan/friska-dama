"""Aimed for testing changes/improvements to the AI. Simply choose which AI 
you want to test and let them battle for any amount of battles you need 
to appropriately test significance in your changes."""

from textUI import get_side_for_humans, game_end_print
from logika import Player, GameState, Gameboard
import csv

#Config variables
player1 = Player.HARD_W
player2 = Player.MEDIUM_B
sample = 100

def pretty_player_print(player):
    """Prints player in a prettier and more informative way for the arena."""
    output = "White " if player >=1 else "Black "

    if abs(Player(player)) == Player.EASY_W:
        difficulty = "Easy"
    elif abs(Player(player)) == Player.MEDIUM_W:
        difficulty = "Medium"
    elif abs(Player(player)) == Player.HARD_W:
        difficulty = "Hard"

    print("Player on the turn:", output + difficulty)

def game(player1, player2, load_game=False):
    """Governs the game process. Takes types of players as its arguments.
       Players 1 and -1 indicate a human player. Players 2 and -2 indicate a computer player.
       In the future, 3 and -3, 4 and -4 etc. might indicate higher difficulties."""
    board = Gameboard(10)
    board.active_player = player1
    board.player1 = player1
    board.player2 = player2
    if load_game:
        board.load_history(load_game)
        board.history = load_game
        #Active player is the one that played in penult turn (= load_game[-2])
        #Player is third item in turn list
        board.active_player = Player(load_game[-2][2])
    board.print()
    turn = 2

    while board.game_state == GameState.Game_on:
        if board.game_state != GameState.Game_on:
            break
        pretty_player_print(board.active_player)
        if board.active_player in (Player.HUMAN_B, Player.HUMAN_W):
            while True:
                figure = getfiguremenu(board, board.active_player, board.player1, board.player2)
                if board.game_state != GameState.Game_on or figure == "changing players":
                    break
                move = getmovemenu(board, board.active_player, figure)
                if move != "change figure":
                    break
        else:
            c_turn = board.computer_turn(board.active_player)
            figure = c_turn[0]
            move = c_turn[1]
        if board.game_state != GameState.Game_on:
            break
        if figure == "changing players":
            continue

        if not board._is_hop(figure, move, board.active_player):
            board.turn(figure, move, board.active_player)
            board.switch_players()
        else:
            board.turn(figure, move, board.active_player)

        if board.has_hopped and not board.can_hop(move, board.active_player):
            board.switch_players()
        turn += 1
        print(f"Battle #{white_win + black_win + draw + 1} "
              f"Turn: {turn//2} "
              f"White wins: {white_win} Black wins: {black_win} Draw: {draw}")

    game_end_print(board)
    board.print()
    return board.game_state

#Initialization of stats variables
white_win = 0
black_win = 0
draw = 0


try:
    for battle in range(sample):
        result = game(player1, player2)
        if result == GameState.White_win:
            white_win += 1
        elif result == GameState.Black_win:
            black_win += 1
        elif result == GameState.Draw:
            draw += 1

        print(f"A battle was fought. #{battle+1}")
except KeyboardInterrupt:
    sample = white_win + black_win + draw
    pass

print(f"White_win: {white_win/sample}",
      f"Black_win: {black_win/sample}",
      f"draw: {draw/sample}",
      f"sample: {sample}",
      f"player1: {player1}",
      f"player2: {player2}",
      )

with open('AI_stats.txt', 'a', newline='') as csvfile:
    cols = ['White_win', 'Black_win', 'draw', 'sample', 'player1', 'player2']
    battle_historian = csv.DictWriter(csvfile, fieldnames=cols, delimiter='\t')
    battle_historian.writerow({'White_win': white_win/sample,
                               'Black_win': black_win/sample,
                               'draw': draw/sample,
                               'sample': sample,    
                               'player1': player1,
                               'player2': player2,
                               })

