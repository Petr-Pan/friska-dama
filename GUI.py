"""Provides a GUI for the game."""

import PyQt5.QtCore as qc
import PyQt5.QtGui as qg
import PyQt5.QtWidgets as qw
from PyQt5 import uic
import json

from logika import Player, FieldError, GameState, Gameboard
from textUI import get_side_for_humans, _assignPlayer
from widgets import BoardWindow, PlayersDialog, UniversalDialog, AIWorker
from widgets.config import CSS_Stylesheet_Edit

class MainWindow(qw.QMainWindow):
    """Provides a main window of the game."""
    def __init__(self):
        self.app = qw.QApplication([])
        super().__init__()
        with open('.\\templates\\main_window.ui') as template:
            uic.loadUi(template, self)

        self.setWindowIcon(qg.QIcon('.\\imgs\\icon.jpg'))

        self.hor_layout = self.findChild(qw.QHBoxLayout, 'horizontalLayout')
        self.board_window = BoardWindow(self)
        self.hor_layout.addWidget(self.board_window)

        self.hint_butt = self.findChild(qw.QPushButton, 'Hint_butt')
        self.hint_butt.pressed.connect(self.hint)

        self.undo_butt = self.findChild(qw.QPushButton, 'Undo_butt')
        self.undo_butt.pressed.connect(self.undo)
        self.undo_QAct = self.findChild(qw.QAction, 'actionUndo')
        self.undo_QAct.triggered.connect(self.undo)

        self.redo_butt = self.findChild(qw.QPushButton, 'Redo_butt')
        self.redo_butt.pressed.connect(self.redo)
        self.redo_QAct = self.findChild(qw.QAction, 'actionRedo')
        self.redo_QAct.triggered.connect(self.redo)

        self.debug_butt = self.findChild(qw.QAction, 'actionConsole')
        self.debug_butt.triggered.connect(self.debug_dialog)

        debug_Qmenu = self.findChild(qw.QMenu, 'menuDebug')
        debug_Qmenu.menuAction().setVisible(False) # Hide debug QMenu

        self.AI_butt = self.findChild(qw.QPushButton, 'AI_butt')
        self.AI_butt.pressed.connect(self.AI_toggle)

        self.save_game_QAct = self.findChild(qw.QAction, 'actionSave_game')
        self.save_game_QAct.triggered.connect(self.save_game)

        self.load_game_QAct = self.findChild(qw.QAction, 'actionLoad_game')
        self.load_game_QAct.triggered.connect(self.load_game)

        self.new_game_QAct = self.findChild(qw.QAction, 'actionNew_game')
        self.new_game_QAct.triggered.connect(self.new_game)

        self.change_players_QAct = self.findChild(qw.QAction, 'actionChange_players')
        self.change_players_QAct.triggered.connect(self.set_players)

        self.change_difficulty_QAct = self.findChild(qw.QAction, 'actionChange_difficulty')
        self.change_difficulty_QAct.triggered.connect(self.set_players)

        self.switch_active_player_QAct = self.findChild(qw.QAction, 'actionSwitch_active_player')
        self.switch_active_player_QAct.triggered.connect(self.board_window.switch_players)

        self.rules_QAct = self.findChild(qw.QAction, 'actionRules')
        self.rules_QAct.triggered.connect(self.rules)

        self.how_to_play_QAct = self.findChild(qw.QAction, 'actionHow_to_play')
        self.how_to_play_QAct.triggered.connect(self.how_to_play)

        self.about_QAct = self.findChild(qw.QAction, 'actionAbout')
        self.about_QAct.triggered.connect(self.about)

        self.active_player_QLabel = self.findChild(qw.QLabel, 'active_player')

        self.history_table = self.findChild(qw.QTableWidget, 'history_table')
        self.history_table.setSortingEnabled(True)
        self.history_table.sortByColumn(0, qc.Qt.DescendingOrder)
        self.history_table.setColumnWidth(0, 10)

        self.thread_pool = qc.QThreadPool()

        self.statusBar().showMessage("Turn computation: ")
        self.stop_computing_butt = qw.QPushButton()
        self.stop_computing_butt.setText("Abort computation")
        self.statusBar().addPermanentWidget(self.stop_computing_butt)

        self.AI_progress_bar = qw.QProgressBar()
        self.statusBar().addPermanentWidget(self.AI_progress_bar)

        self.AI_setup()
        self.board_window.show()

    def run(self):
        """Runs the app."""
        self.show()
        return self.app.exec()

    def new_game(self):
        """Starts a new game."""
        PlayersDialog(self, self.board_window, True)

    def save_game(self):
        """Saves a game into a file."""
        if len(self.board_window.gameboard.history) < 2:
            UniversalDialog(self, "Game too short",
                            "Cannot save game with less than two turns.")
            return False

        save, _ = qw.QFileDialog.getSaveFileName(self, 'Save game', directory='Saves', filter='*.json')

        if save:
            self.board_window.gameboard.save_game(save)

    def load_game(self):
        """Loads a game from a file."""
        save, _ = qw.QFileDialog.getOpenFileName(self, 'Load game', directory='Saves', filter='*.json')

        if not save: # If dialog gets closed without selecting a file
            return False

        try:
            with open(save, "r") as file:
                new_board = self.board_window.gameboard.load_game(file)

        except FileNotFoundError:
            UniversalDialog(self, "File not found"
                            "Game could not be loaded: file not found.")
            return False 
        except (IndexError, json.decoder.JSONDecodeError) as e:
            UniversalDialog(self, "File corrupted",
                            "Game could not be loaded: file is corrupted.\n" \
                                f"{e}")
            return False
        except ValueError as e:
            UniversalDialog(self, "File corrupted",
                            "Game could not be loaded: file is either corrupted or contains illegal moves.\n" \
                                f"{e}")
            return False

        else:
            self.board_window.gameboard = new_board
            self.board_window.selected_figure = []
            self.board_window.update()
            self.active_player_update()
            self.history_update()
            self.AI_setup()
            return True

    def set_players(self):
        """Sets players of the gameboard to the ones set via created PlayersDialog."""
        PlayersDialog(self, self.board_window, False)

    def hint(self):
        """Computes and displays best move for the active player."""
        if not self.board_window.AI_worker and self.board_window.gameboard.game_state == GameState.Game_on:
            self.board_window.compute_hint()
            self.statusBar().show()
        
    def undo(self):
        """Undoes a turn of the gameboard."""
        self.board_window.gameboard.undo()
        self.active_player_update()

        if self.board_window.gameboard.undocounter < len(self.board_window.gameboard.history):
            self.history_table.selectRow(self.board_window.gameboard.undocounter)
            
        else: # Player has undoed to the start position -> no turn to highlight
            self.history_table.clearSelection()
                       
        # If AI is set, then undo should turn it off for a while
        # Else, AI would play right after pressing undo, unpleasant
        if self.board_window.AI_timer.isActive():
            self._AI_turn_off()
        elif not self.board_window.AI_timer.isActive() and self.board_window.AI_worker:
            self.board_window.AI_worker.abort()
            self._AI_turn_off()

        self.board_window.update()

    def redo(self):
        """Redoes a turn of the gameboard."""
        self.board_window.gameboard.redo()
        self.active_player_update()
        self.history_table.selectRow(self.board_window.gameboard.undocounter)
        
        self.board_window.update()

    def rules(self):
        """Displays a dialog with rules of the game."""
        UniversalDialog(self, "Rules", _read_from_doc('.\\docs\\rules.rst'), 400, 400)
    
    def how_to_play(self):
        """Displays a dialog explaining how to play."""
        UniversalDialog(self, "How to play", _read_from_doc('.\\docs\\how_to_play.rst'), 400, 400)

    def about(self):
        """Displays a dialog with information about the game."""
        UniversalDialog(self, "About", _read_from_doc('.\\docs\\about.rst'), 400, 125)

    def debug_dialog(self, event):
        """An eval() dialog for debugging during development."""
        dialog = DebugDialog(self)

    def active_player_update(self):
        self.active_player.setText(f"Active player: "
                                   f"{get_side_for_humans(self.board_window.gameboard.active_player)}")

    def history_update(self):
        """Updates the history table."""
        self.history_table.setRowCount(0)
        row = 1
        self.history_table.setSortingEnabled(0)

        for turn in self.board_window.gameboard.history:
            self.history_table.setRowCount(row)
            
            # Integers needs to be set this way, 
            # otherwise sorting by them is done alphanumerically by Qt
            row_item = qw.QTableWidgetItem()
            row_item.setData(qc.Qt.EditRole, qc.QVariant(row))
                        
            self.history_table.setItem(row - 1,
                                   0,
                                   qw.QTableWidgetItem(row_item))
            self.history_table.setItem(row - 1, 1,
                                   qw.QTableWidgetItem(str(get_side_for_humans(turn[2]))))
            self.history_table.setItem(row - 1, 2, qw.QTableWidgetItem(str(turn[0])))
            self.history_table.setItem(row - 1, 3, qw.QTableWidgetItem(str(turn[1])))
                                   
            row += 1

        self.history_table.setSortingEnabled(1)
        self.history_table.selectRow(0)

    def AI_setup(self):
        """Configures AI functionality depending on whether AI player
        is present. Needs to be called on new/loaded game."""
        if (self.board_window.gameboard.player1 not in (Player.HUMAN_W, Player.HUMAN_B)
            or self.board_window.gameboard.player2 not in (Player.HUMAN_W, Player.HUMAN_B)):
            
            self.AI_butt.setEnabled(True)
            self._AI_turn_on()
            self.statusBar().show()

        else:
            self.board_window.AI_timer.stop()
            self.AI_butt.setEnabled(False)
            self.AI_butt.setText('No AI')
            self.AI_butt.setStyleSheet(CSS_Stylesheet_Edit.AI_butt_disabled)
        
    def AI_toggle(self):
        """Pauses/starts AI turns."""
        if self.board_window.AI_timer.isActive():
            self._AI_turn_off()

        elif not self.board_window.AI_timer.isActive() and self.board_window.AI_worker:
            self.board_window.AI_worker.abort()
            self._AI_turn_off()

        else:
            self._AI_turn_on()

    def _AI_turn_off(self):
        """Turns off AI_timer and updates AI_butt accordingly."""
        self.board_window.AI_timer.stop()
        self.AI_butt.setText('AI off')
        self.AI_butt.setStyleSheet(CSS_Stylesheet_Edit.AI_butt_off)

    def _AI_turn_on(self):
        """Turns on AI_timer and updates AI_butt accordingly."""
        self.board_window.AI_timer.start()
        self.AI_butt.setText('AI on')
        self.AI_butt.setStyleSheet(CSS_Stylesheet_Edit.AI_butt_on)
            


class DebugDialog(qw.QDialog):
    """Dialog for debug during runtime. Hidden by default.
    Press Ctrl+D to open it during open GUI game.
    Game needs to be started by GUI.py so that main_window is in scope."""
    def __init__(self, window):
        super().__init__(window)
        
        with open('.\\templates\\debug_dialog.ui') as template:
            uic.loadUi(template, self)
     
        self.eval_butt = self.findChild(qw.QPushButton, 'eval_butt')
        self.eval_butt.pressed.connect(self.eval_input)

        self.input = self.findChild(qw.QTextEdit, 'code_input')

        self.show()

    def eval_input(self):
        """Evaluates string input for easier debugging during runtime.
        Discard when game ready for release."""
        try:
            eval(self.input.toPlainText())
        except Exception as e:
            print(e)
            pass

def _read_from_doc(path):
    """Converts Sphinx-friendly rst file to UniversalDialog-friendly string."""
    with open(path) as source:
        text = source.readlines()[5:]
        separator = ''
        text = separator.join(text)
    return text


if __name__ == '__main__':
    main_window = MainWindow()
    main_window.run()
