# Friska dama

Project for Projektový seminář 1-2 (KMI/YPS1-2). Planned to be a full game with GUI in a year.

To start the game, run dama.py.

Filenames starting with "test_" indicate files that are meant to be run with pytest. These files contain tests only.

Master branch contains last stable release. Dev branch is for storing work in progress and may (will) contain bugs, errors or various unexpected behaviour.

To access documentation, open index.html in /docs/\_build/html folder.