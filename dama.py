"""Main file for starting the game."""
from GUI import MainWindow


if __name__ == '__main__':
    main_window = MainWindow()
    main_window.run()
