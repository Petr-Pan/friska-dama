.. _AI_arena:

AI_arena module
===============

Intro
-----
AI_arena is a module aimed for testing changes/improvements to the AI.
Simply choose which AI you want to test and let them battle for any amount of battles
you need to appropriately test significance in your changes.

Config
------
As the module was originally supposed for internal testing only, it does not provide any UI
for configuration - all config must be done directly in the source code of the module.

The plus side is that all you need to config is the sample size (amount of battles you wish to simulate),
defined by the variable "sample", and which computer players you wish to pit against each other,
defined by variables "player1" and "player2" (use Player class from logika module).

These config variables are located at the beginning of the file, after module docstring and imports.

Output
------
By default, the module prints basic statistics of the fights in the consoleand exports them into file AI_stats.txt as csv.

If you do not need any console output, comment out the print() calls in the game() function.
Same with csv exports which is done by battle_historian.writerow(args) call.

Game() function
---------------

Takes care of running single instances of the game and continuous monitoring output in the console.
It is basically the same function as in the textUI module, but it was duplicated for ease of use
and configuring printing outputs.

.. function:: AI_arena.game