.. _rules:

Rules
======

'Friska dama' is a historic variant of dama (or checkers/draughts). Mostly it is similar to classic dama, with few notable exceptions.

Starting board
--------------
Figures can only stand on white fields. The game board is a 10x10 checkerboard. At start, top four rows are populated with black figures, bottom four with white figures.

These basic figures are called peons in this package.

Game itself
-----------
Peons can move only forward to the enemy side, only by one field and only diagonally. Exception to this rule are hops.

A hop can occur when a figure has an enemy figure in an adjacent light field and when there is no figure behind said enemy figure.
The enemy figure then can be hopped over which removes it from the board. Note that hopping is required whenever possible.

One of the main things that makes friska dama different is that hop can occur in every possible way, even with peon figures - you can hop diagonally, orthogonally, even backwards!

When a peon gets to the end of the board on the enemy side, it changes into a lady figure. Ladies can move in any straight way - no hopping needed - and can cross as many fields as they want as long as the target field is empty and following conditions are fulfilled:

If a hop is possible, it must be made. You can jump over any number of empty fields but only over a single enemy figure per hop. With this in mind, it does not matter how many empty fields are jumped over. Your own figures cannot be jumped over at all. Jumping to dark fields of the board is still forbidden.

This brings us to multihops. A multihop is when you make consencutive hops. Multihops are eligible when the figure you have hopped with can hop again. There is no top limit on the amount of times you can multihop, as long as you can hop over another figure with the hopping figure. Both ladies and peons can multihop.


Game end
----------
The player that takes or blocks all of the enemy figures wins the game.

If no hop occurs for more than 30 turns, the game ends as a draw.
