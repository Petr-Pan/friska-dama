.. Friska dama documentation master file, created by
   sphinx-quickstart on Sun Feb  3 17:47:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Friska dama's documentation!
=======================================

This page covers a basic overview of this game. So far TLI only, planned to be a full game with GUI in a year.

This game is a variation on a board game called *checkers* or *dama* in Czech (hence the name Friska dama).

Friska dama - differences
-------------------------
Main difference is that peon figures (ordinary stones) can hop over enemy figures not only diagonally, but also vertically
and horizontally, even backwards! No-hop moves are still diagonal-only. Lady figures are also introduced
(also known as queen, king or dame). These can move in any straight-line even when not hopping. For other rule changes,
see :ref:`Rules`.

Installation and repository overview
------------------------------------
To start the game, run dama.py.

Filenames starting with \"test\_\" indicate files that are meant to be run with pytest. These files contain tests only.

Master branch contains last stable release. Dev branch is for storing work in progress and may (will) contain bugs, errors or various unexpected behaviour.

Repository can be found `here <https://bitbucket.org/Petr-Pan/friska-dama/src/master/>`_.

Code documentation
------------------
For information on what's under the hood see :ref:`logika`, for TLI documentation see :ref:`textUI`.

Features
--------

- a TLI based checkers game
- hot-seat PvP mode
- PvE mode
- spectator mode
- games can be saved and loaded (even cheated) using json

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   logika
   textUI
   rules
   how_to_play
   about
   AI_arena

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
