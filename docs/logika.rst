.. _logika:

Logika module
=============

.. module:: logika

Intro
-----

This module covers basics of the game itself - board generation, board management and enforces game rules.

To properly play, this module needs to be connected with a TLI or GUI module which will call upon appropriate methods.

Gameboard class
---------------

.. autoclass:: logika.Gameboard
   :members:
   
Enums and helping function
--------------------------
.. autoclass:: logika.FieldError
   :members:
   
.. autoclass:: logika.GameState
   :members:
 
.. autoclass:: logika.Player
