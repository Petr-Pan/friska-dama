.. _textUI:

TextUI module
=============

.. module:: textUI

Intro
-----

This module takes care of TLI communication between the game board and human player(s).

Menu functions
---------------

These functions provide various menus to choose various game options and paths.

.. autofunction:: textUI.start

.. autofunction:: textUI.newgamemenu

.. autofunction:: textUI.loadgamemenu

.. autofunction:: textUI.PvE_menu

.. autofunction:: textUI.EvE_menu

.. autofunction:: textUI.getfiguremenu

.. autofunction:: textUI.getmovemenu

.. autofunction:: textUI.savegamemenu
   
Game function
-------------

.. autofunction:: textUI.game


Helping functions
-----------------
.. autofunction:: textUI.loadgame

.. autofunction:: textUI.game_end_print

.. autofunction:: textUI._assignPlayer

.. autofunction:: textUI._assignPlayer_check

.. autofunction:: textUI._validateside

.. autofunction:: textUI.get_side_for_humans