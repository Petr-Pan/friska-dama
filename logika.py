"""Contains game logic for textUI module. Provides a board, AI and game logic
according to friska dama rules."""

from statistics import mean
import json
import copy
import random
from enum import IntEnum


class Player(IntEnum):
    """Enum for possible types of players.
    W = white figure player
    B = black figure player."""
    EASY_W = 1
    EASY_B = -1
    MEDIUM_W = 3
    MEDIUM_B = -3
    HARD_W = 5
    HARD_B = -5
    HUMAN_W = 42
    HUMAN_B = -42

class FieldError(IntEnum):
    """Enum for errors that can happen on the board."""
    NotOnBoard = 998
    DarkField = 999
    EmptyField = 0
    InvalidInput = 997

class _Way(IntEnum):
    """Used for private methods for peon moving."""
    Left_diag = -1
    Right_diag = 1
    Left_ortho = -2
    Right_ortho = 2

class _WayFormulas(object):
    """Provides formulas for computation of moving in different ways on the board."""
    Up = '[figure[0] - 2, figure[1]]'
    Down = '[figure[0] + 2, figure[1]]'
    Left = '[figure[0], figure[1] - 2]'
    Right = '[figure[0], figure[1] + 2]'
    Up_left = '[figure[0] - 1, figure[1] - 1]'
    Up_right = '[figure[0] - 1, figure[1] + 1]'
    Down_right = '[figure[0] + 1, figure[1] + 1]'
    Down_left = '[figure[0] + 1, figure[1] - 1]'

class GameState(IntEnum):
    """Enum for states of the game. Game_on means the game is in valid progress.
    Class Gameboard automatically changes it's game_state attribute if game
    progress resulted in a change of state, e.g. white player won."""
    Draw = 0
    White_win = 1
    Black_win = -1
    Game_on = 42
    Exit = 999

class Gameboard:
    """Core of the module - creates a gaming board, controls figure movement
    and takes care of enforcing all appropriate checkers rules."""
    def __init__(self, size):
        self._create_empty_board(size)
        self._create_starting_board(size)
        self.board_size = size
        #self.figurerows = size/2 - 2
        self.game_state = GameState.Game_on
        self.history = []
        self.undocounter = 0
        self.turns_since_hop = 0
        self.player1 = Player.HUMAN_W
        self.player2 = Player.HUMAN_B
        self.active_player = self.player1
        self.has_hopped = []

    def _create_empty_board(self, size):
        """Creates an empty board without figures. Takes one argument
        that sets the number of columns and rows."""
        self.board = []
        i = 1
        for row_index in range(size):
            row = []
            for col_index in range(size):
                if i % 2 == 0:
                    row.append(0)
                else:
                    row.append("X")
                i += 1
            i += 1
            self.board.append(row)

    def _create_starting_board(self, size):
        """Places figures on the board. Argument "size" sets the number of columns
        where figures will be placed."""
        for row in range(4):
            for field in range(size):
                if self.board[row][field] == 0:
                    self.board[row][field] = -1
        for row in range(9, 5, -1):
            for field in range(size):
                if self.board[row][field] == 0:
                    self.board[row][field] = 1

    def print(self):
        """Prints the gameboard."""
        printcopy = copy.deepcopy(self.board)
        #Add an empty space for better readability
        i = 0
        for row in range(self.board_size):
            for field in range(self.board_size):
                if printcopy[row][field] == 0:
                    printcopy[row][field] = ' 0'
                if printcopy[row][field] == 1:
                    printcopy[row][field] = ' 1'
                if printcopy[row][field] == 'X':
                    printcopy[row][field] = ' X'
        for row in printcopy:
            #Adds row indexes
            print(i, end=': ')
            i += 1
            #Prints the fields
            for field in row:
                print(field, end=' ')
            print()
        #Make a pretty line between the board and horizontal indexes
        print('    ----------------------------')
        print('    ', end='')
        #Add horizontal indexes
        for j in range(self.board_size):
            print(j, end='  ')
        print()

    def save_game(self, file):
        """Saves game into file. Returns [0, 0] if successful."""
        try:
            with open(file, "w") as fp:
                json.dump(self.history, fp)
        except FileExistsError as e:
            return [FileExistsError, e]
        except OSError as e:
            return [OSError, e]
        else:
            return [0, 0]

    def load_game(self, file):
        """Loads tuples of figures and moves from outer file and changes board accordingly."""
        history = json.load(file)
        for turn in history:
            turn[2] = Player(turn[2])
        last_turn = history[-1]
        penult_turn = history[-2]
        player1 = last_turn[2]
        player2 = penult_turn[2]
                
        if player1/abs(player1) == player2/abs(player2): # Last few turns may have been multihops
            offset = 2 # We stopped searching for player 2 in history[-2]
            while player1/abs(player1) == player2/abs(player2): # Find last turn of player 2
                offset += 1
                player2 = history[-offset][2]

        if player1 < player2:
            player1, player2 = player2, player1
        
        new_board = Gameboard(10)
        new_board.player1 = player1
        new_board.player2 = player2
        new_board.active_player = player1

        # Set second argument to False to allow illegal moves
        new_board.load_history(history, True)
        new_board.history = history

        return new_board

    def load_history(self, history, enforce_legal_moves=False):
        """Changes board state to the 'history state'. Takes a list of turns as an argument.
        A turn is a list of figure, its move and player."""
        self._create_empty_board(self.board_size)
        self._create_starting_board(self.board_size)
        self.game_state = GameState.Game_on
        self.turns_since_hop = 0
        self.active_player = self.player1
        self.has_hopped = []

        for turn in history:
            if enforce_legal_moves:
                if not self.move_ok(turn[0], turn[1], turn[2]):
                    raise ValueError(f"Invalid value in {turn}")            
            if not self._is_hop(turn[0], turn[1], turn[2]):
                self.turn(turn[0], turn[1], turn[2], False)
                self.switch_players()
            else:
                self.turn(turn[0], turn[1], turn[2], False)

            if self.has_hopped and not self.can_hop(turn[1], self.active_player):
                self.switch_players()

    def turn(self, figure, move, player, history=True):
        """Governs the turn process."""
        if self._is_hop(figure, move, player):
            self.has_hopped = move # Figure that has hopped is used in multihop computing
        else:
            self.has_hopped = []

        if self._stone_taking(figure, move, player):
            self.turns_since_hop = 0
        else:
            self.turns_since_hop += 1
            
        played_figure = self.board[figure[0]][figure[1]]
        self.board[figure[0]][figure[1]] = 0
        if self._should_become_lady(move, player):
            self.board[move[0]][move[1]] = 1.5 * self.get_side(player)
        else:
            self.board[move[0]][move[1]] = played_figure

        if history:
            if self.undocounter != 0:
                self.history = self.history[:-self.undocounter]
            self.history.append([figure, move, player])
            self._reset_undocounter()

        #Prints last turn for debugging
        #self.print_last_turn(figure, move, player)

    def print_last_turn(self, figure, move, player):
        """Used in TLI for debugging last turn. Uncomment call in turn method for use.
        Will be deprecated once UI is implemented."""
        last_turn = Gameboard(10)
        last_turn.board = []
        for row in self.board:
            last_turn.board.append(row.copy())
        last_turn.board[figure[0]][figure[1]] = "[" + str(self.board[figure[0]][figure[1]]) + "]"
        last_turn.board[move[0]][move[1]] = "|" + str(self.board[move[0]][move[1]]) + "|"
        last_turn.print()
        print("Player that was on turn:", player)

    def undo(self):
        """Undos a half turn. (Only the turn of last player, not both.)"""
        if self.undocounter < len(self.history):
            self.load_history(self.history[:-self.undocounter-1])
            self.undocounter += 1

    def redo(self):
        """Redoes an undoed half turn."""
        if self.undocounter > 0:
            turn = self.history[-self.undocounter]
            if not self._is_hop(turn[0], turn[1], turn[2]):
                self.turn(turn[0], turn[1], turn[2], False)
                self.switch_players()
            else:
                self.turn(turn[0], turn[1], turn[2], False)

            if self.has_hopped and not self.can_hop(turn[1], self.active_player):
                self.switch_players()
            self.undocounter -= 1

    def _reset_undocounter(self):
        """Sets undocounter to zero."""
        self.undocounter = 0

    def switch_players(self):
        """Used to switch players."""
        if self.active_player == self.player1:
            self.active_player = self.player2
        elif self.active_player == self.player2:
            self.active_player = self.player1
        else:
            raise ValueError("Invalid player on board:", self.active_player)

        self.has_hopped = []
        self.can_play(self.active_player)

    def _set_active_player_from_history(self, past_player):
        """Players can be changed during the game. But if an undo is called
        beyond the turn where this happened, old player before the change would
        be set as active, crashing the game. This method correctly sets the active
        player to the one after the change."""
        if past_player/abs(past_player)  == self.player1/abs(self.player1):
            self.active_player = self.player1
        elif past_player/abs(past_player) == self.player2/abs(self.player2):
            self.active_player = self.player2
        else:
            raise ValueError("Invalid player in game history. If you loaded from a file," \
                             "check if the file has not been tampered with.")

    def _should_become_lady(self, move, player):
        """Returns true if figure of a player got to the other side and should become lady."""
        if self.get_side(player) == 1 and move[0] == 0:
            return True
        elif self.get_side(player) == -1 and move[0] == 9:
            return True
        else:
            return False

    def can_play(self, active_player, set_game_state=True):
        """Checks if a given player can still legally play."""
        #Check if game ends because of no hop in 30 turns (60 half-turns)
        if self.turns_since_hop == 60:
            if set_game_state:
                self.game_state = GameState.Draw
            return GameState.Draw

        #Check if player has legal moves to play
        if self._has_moves(active_player):
            return True
        #If the player cannot play, let's see who won
        else:
            return self._check_game_result(set_game_state)

    def _check_game_result(self, set_game_state=True):
        """Determines who won depending on which player is active
        and has no valid moves left and therefore got blocked/eliminated."""
        if self.active_player >= 1:
            game_state = GameState.Black_win
        elif self.active_player <= -1:
            game_state = GameState.White_win
        else:
            raise ValueError(f"Invalid active player: {self.active_player}")

        if set_game_state:
            self.game_state = game_state

        return game_state

    def _check_input(self, figure):
        """Validizes input and returns appropriate errors or True if input correct."""
        try:
            self.board[figure[0]][figure[1]] > 0
        except TypeError:
            try:
                self.board[figure[0]][figure[1]] == "X"
            except TypeError:
                #TypeError here means invalid input was passed - coords point nowhere
                return FieldError.InvalidInput
            #If coords cannot be compared with 0, but are valid -> they point to "X" field
            return FieldError.DarkField
        except IndexError:
            #Selected field is out of bounds - not on board
            return FieldError.NotOnBoard
        else:
            return True

    def is_players_figure(self, figure, player):
        """Returns 1 if figure belongs to the player.
           returns -1 if figure belongs to the other player.
           returns 0 if empty field.
           returns FieldError.DarkField if not a figure but a dark field.
           returns FieldError.NotOnBoard if the the figure is out of range.
           returns FieldError.InvalidInput if invalid input was passed."""
        check = self._check_input(figure)
        if check != True:
            return check
        board_figure = self.board[figure[0]][figure[1]]
        if board_figure > 0 and player >= 1:
            return 1
        elif board_figure < 0 and player <= -1:
            return 1
        elif board_figure == 0:
            return 0
        else:
            return -1

    def move_ok(self, figure, move, player):
        """So far only checks whether the move is within map range
        and that the target field is not a dark field."""
        try:
            int(self.board[move[0]][move[1]])
        except IndexError:
            return False
        except ValueError:
            #Target field is a dark field - "X"
            return False

        return [figure, move] in self._move_generator(player)

    def possible_moves(self, figure, player):
        """Returns all possible moves for a given figure."""
        figure_type = abs(self.get_field(figure))
        output = []
        if figure_type == 1:
            output.extend(self._possible_moves_peon_diag(figure, player, _Way.Left_diag))
            output.extend(self._possible_moves_peon_diag(figure, player, _Way.Right_diag))
            output.extend(self._possible_moves_peon_orthog_vert(figure, player))
            output.extend(self._possible_moves_peon_orthog_hor(figure, player, _Way.Left_ortho))
            output.extend(self._possible_moves_peon_orthog_hor(figure, player, _Way.Right_ortho))

            return output
        elif figure_type == 1.5:
            return self._possible_moves_lady(figure, player)
        else:
            raise ValueError("Some thieving elf got a nonexisting figure on board! Crashing!")

    def _possible_moves_peon_diag(self, figure, player, way):
        """Returns all possible moves in a given diagonal way of a given peon."""
        output = []
        #Add potential step
        if self._figure_on_diag_step(figure, player, way) == 0:
            output.append(self._diag_step(figure, player, way))

        #Add potential diagonal hop
        enemy_figure = self._figure_on_diag_step(figure, player, way)
        #Check if in variable enemy_figure is truly an enemy figure
        if self.get_side(enemy_figure) == self.get_side(player) * -1:
            if self._figure_on_diag_hop_step(figure, player, way) == 0:
                output.append(self._diag_hop_step(figure, player, way))

        #Peons can hop backwards - let's add these too
        #Diagonal backwards hop
        enemy_figure_back_diag = self._figure_on_diag_step(figure, player * -1, way)
        if self.get_side(enemy_figure_back_diag) == self.get_side(player) * -1:
            if self._figure_on_diag_hop_step(figure, player * -1, way) == 0:
                output.append(self._diag_hop_step(figure, player * -1, way))

        return output

    def _possible_moves_peon_orthog_vert(self, figure, player):
        """Returns all possible moves of a peon in a vertical way."""
        output = []
        #Add orthogonal hop
        enemy_figure_ort = self._figure_on_orthog_vert_step(figure, player)
        if self.get_side(enemy_figure_ort) == self.get_side(player) * -1:
            if self._figure_on_orthog_vert_hop_step(figure, player) == 0:
                output.append(self._orthog_vert_hop_step(figure, player))

        #Orthogonal backwards hop
        enemy_figure_back_ort = self._figure_on_orthog_vert_step(figure, player * -1)
        if self.get_side(enemy_figure_back_ort) == self.get_side(player) * -1:
            if self._figure_on_orthog_vert_hop_step(figure, player * -1) == 0:
                output.append(self._orthog_vert_hop_step(figure, player * -1))

        return output

    def _possible_moves_peon_orthog_hor(self, figure, player, way):
        """Returns all possible moves of a peon in a given horizontal way."""
        output = []

        enemy_figure_hor = self._figure_on_orthog_hor_step(figure, way)
        if self.get_side(enemy_figure_hor) == self.get_side(player) * -1:
            if self._figure_on_orthog_hor_hop_step(figure, way) == 0:
                output.append(self._orthog_hor_hop_step(figure, way))
        
        return output

    def _diag_step(self, figure, player, way):
        """Returns diagonal step for a given figure.
        When way == 1, step will be to the right.
        when way == -1, step will be to the left."""
        return self._check_step([figure[0] - self.get_side(player), figure[1] + way])

    def _diag_hop_step(self, figure, player, way):
        """Returns diagonal hopping step for a given figure."""
        return self._check_step([figure[0] - (2 * self.get_side(player)), figure[1] + (2 * way)])

    def _orthog_vert_step(self, figure, player):
        """Returns orthogonal, vertical step for a given figure."""
        return self._check_step([figure[0] - (2 * self.get_side(player)), figure[1]])

    def _orthog_vert_hop_step(self, figure, player):
        """Returns orthogonal, vertical hopping step for a given figure."""
        return self._check_step([figure[0] - (4 * self.get_side(player)), figure[1]])

    def _orthog_hor_step(self, figure, way):
        """Returns orthogonal, vertical step for a given figure."""
        return self._check_step([figure[0], figure[1] + way])

    def _orthog_hor_hop_step(self, figure, way):
        """Returns orthogonal, vertical hopping step for a given figure."""
        return self._check_step([figure[0], figure[1] + (2 * way)])

    def _figure_on_diag_step(self, figure, player, way):
        """Returns figure on board that is on diagonal step."""
        return self.get_field([self._diag_step(figure, player, way)[0],
                               self._diag_step(figure, player, way)[1]])

    def _figure_on_diag_hop_step(self, figure, player, way):
        """Returns figure on board that is on a diagonal hopping step."""
        return self.get_field([self._diag_hop_step(figure, player, way)[0],
                               self._diag_hop_step(figure, player, way)[1]])

    def _figure_on_orthog_vert_step(self, figure, player):
        """Returns figure on board that is on orthogonal step."""
        return self.get_field([self._orthog_vert_step(figure, player)[0],
                               self._orthog_vert_step(figure, player)[1]])
    
    def _figure_on_orthog_vert_hop_step(self, figure, player):
        """Returns figure on board that is on a orthogonal hopping step."""
        return self.get_field([self._orthog_vert_hop_step(figure, player)[0],
                               self._orthog_vert_hop_step(figure, player)[1]])

    def _figure_on_orthog_hor_step(self, figure, way):
        """Returns figure on board that is on orthogonal step."""
        return self.get_field([self._orthog_hor_step(figure, way)[0],
                               self._orthog_hor_step(figure, way)[1]])
    
    def _figure_on_orthog_hor_hop_step(self, figure, way):
        """Returns figure on board that is on a orthogonal hopping step."""
        return self.get_field([self._orthog_hor_hop_step(figure, way)[0],
                               self._orthog_hor_hop_step(figure, way)[1]])

    def _possible_moves_lady(self, figure, player):
        """Returns all possible moves for a lady figure."""
        output = []
        #Up
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Up))
        #Down
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Down))
        #Left
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Left))
        #Right
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Right))
        #Diag Up-left
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Up_left))
        #Diag Up-right
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Up_right))
        #Diag Down-right
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Down_right))
        #Diag Down-left
        output.extend(self._possible_moves_lady_calc(figure, player, _WayFormulas.Down_left))
                
        return output

    def _possible_moves_lady_calc(self, figure, player, formula):
        """Calculates all possible moves through a given formula.
        Formula should be a list computing next field in sequence of a desired way.
        
        E.g. if figure == [5,4] and we want to calculate coords of next figures above (Way == Up),
        formula would be '[figure[0] - 2, figure[1]]', resulting in fields [3,4] and [1,4]
        which are correct playable fields above figure [5,4].
        
        This method takes all fields in thusly computed way and adds them to pool of
        possible moves if they are in accord with the rules."""
        output = []
        figure = eval(formula)
        met_the_enemy = False # Holds information whether the lady has hopped over an enemy figure

        while self.is_players_figure(figure, player) <= 0:
            if self.is_players_figure(figure, player) == 0:
                output.append(figure)
            elif self.is_players_figure(figure, player) == -1 and not met_the_enemy:
                met_the_enemy = True
            elif self.is_players_figure(figure, player) == -1 and met_the_enemy:
                break

            figure = eval(formula)
            if self._check_step(figure)[0] is False:
                break
        return output

    def _stone_taking(self, figure, move, player, is_check=False):
        """Removes enemy figures during hops by calling appropriate method for given figure."""
        figure_type = abs(self.get_field(figure))
        if figure_type == 1:
            return self._stone_taking_peon(figure, move, player, is_check)
        elif figure_type == 1.5:
            return self._stone_taking_lady(figure, move, player, is_check)
        elif figure_type == 0:
            return False
        else:
            raise ValueError("Thieving elves are at it again!"
                             "Nonsensical figure on board, crashing")

    def _stone_taking_peon(self, figure, move, player, is_check):
        """Governs stone taking by peon figures."""
        middle_figure = [mean([figure[0], move[0]]), mean([figure[1], move[1]])]
        if self.is_players_figure(middle_figure, player) == -1:
            if not is_check:
                self.board[middle_figure[0]][middle_figure[1]] = 0
            return True
        else:
            return False

    def _stone_taking_lady(self, figure, move, player, is_check):
        """Governs stone taking by lady figures."""
        has_taken = False
        #Compute which way the lady will travel - e.g. row_way = 1, col_way = -1 means
        #that the lady will travel diagonally to down-left
        if move[0] - figure[0] == 0:
            row_way = 0
        else:
            row_way = (move[0] - figure[0])//abs((move[0] - figure[0]))

        if move[1] - figure[1] == 0:
            col_way = 0
        else:
            col_way = (move[1] - figure[1])//abs((move[1] - figure[1]))

        #While the lady reaches her destination, remove all enemy figures in her way
        while figure != move:
            if self.is_players_figure(figure, player) == -1:
                if not is_check:
                    self.board[figure[0]][figure[1]] = 0
                has_taken = True
            figure = [figure[0] + row_way, figure[1] + col_way]
        return has_taken

    def computer_turn(self, active_player):
        """Calls appropriate AI for given active_player."""
        if active_player in (Player.EASY_B, Player.EASY_W):
            return self.computer_turn_easy(active_player)
        elif active_player in (Player.MEDIUM_B, Player.MEDIUM_W):
            return self.computer_turn_medium(active_player)
        elif active_player in (Player.HARD_B, Player.HARD_W):
            return self.computer_turn_hard(active_player)

    def computer_turn_easy(self, active_player):
        """Computes turn for easy difficulty."""
        turn_pool = self._move_generator(active_player)
        #Choose a random move and return it
        return turn_pool[random.randint(0, len(turn_pool)-1)]
    
    def computer_turn_medium(self, active_player):
        """Computes turn for medium difficulty."""
        return best_move(self, active_player, 1)

    def computer_turn_hard(self, active_player):
        """Computes turn for hard difficulty."""
        return best_move(self, active_player, 3)

    def _move_generator(self, active_player):
        """Generates valid moves for figures of passed player.
        If there are possible hops, returns only hop moves."""
        turn_pool = []

        if self.has_hopped:
            moves = self.possible_moves(self.has_hopped, active_player)
            if moves:
                for move in moves:
                    turn_pool.append([self.has_hopped, move])

        else:
            my_figures = self._find_my_figures(active_player)
            #Get a list of possible moves for all figures
            for figure in my_figures:
                moves = self.possible_moves(figure, active_player)
                if moves:
                    for move in moves:
                        turn_pool.append([figure, move])

        #Look for hop moves
        hop_moves = []
        for turn in turn_pool:
            if self._is_hop(turn[0], turn[1], active_player):
                hop_moves.append(turn)

        #If can hop, return hop moves, else normal steps
        if hop_moves:
            return hop_moves
        else:
            return turn_pool
    
    def _find_my_figures(self, player):
        """Returns all figures that a given player has on board."""
        my_figures = []
        for row in range(self.board_size):
            for field in range(self.board_size):
                if self.is_players_figure([row, field], player) == 1:
                    my_figures.append([row, field])

        return my_figures

    def _appraise_board(self):
        """Appraises the current board state for further use in minimax.
        Appraisal > 0 means that white player is in better position.
        Appraisal < 0 means that black player is in better position."""
        appraisal = 0

        #Count all black and white figures on the board
        white_figures = self._find_my_figures(1)
        for figure in white_figures:
            appraisal += self.get_field(figure) * 10

        black_figures = self._find_my_figures(-1)
        for figure in black_figures:
            appraisal += self.get_field(figure) * 10

        for figure in white_figures:
            #Figures on the side cannot be hopped -> worth more
            if figure[1] == 0 or figure[1] == 9:
                appraisal += 1

            #Figures on the back cannot be hopped -> worth more
            if figure[0] == 0 or figure[0] == 9:
                appraisal += 1.5

        for figure in black_figures:    #Redo the same for enemy figures
            if figure[1] == 0 or figure[1] == 9:
                appraisal -= 1

            if figure[0] == 0 or figure[0] == 9:
                appraisal -= 1.5

        return appraisal

    def _has_moves(self, player):
        """Returns all possible_moves for player's figures."""
        my_figures = self._find_my_figures(player)
        has_moves = []
        if self.has_hopped:
            has_moves.append(self.possible_moves(self.has_hopped, player))
        else:
            for figure in my_figures:
                has_moves.append(self.possible_moves(figure, player))
        has_moves = [x for x in has_moves if x != []]
        return has_moves
    
    def _is_hop(self, figure, move, player):
        """Returns True if the move is a hop."""
        return self._stone_taking(figure, move, player, True)

    def get_side(self, player):
        #<><><><><><< Consider turning this into a function - no self used
        """To get simplified player side, so it can be used in further computing.
            Returns 1 for player with positive figures,
            returns -1 for player with negative figures."""
        if player in (0, None, 'X'):
            return 0
        return int(player/abs(player))

    def get_field(self, coords):
        """Accepts coords in the form of a list, e.g. [0,1] will return
        field that is on first row, second column."""
        try:
            return self.board[coords[0]][coords[1]]
        except IndexError:
            return False
   
    def _check_step(self, figure):
        """Checks if given figure (coords) are still on the gameboard."""
        if 0 <= figure[0] < self.board_size and 0 <= figure[1] < self.board_size:
            return figure
        else:
            return [False, False]

    def can_hop(self, figure, player):
        """Returns true if the figure can hop."""
        for move in self.possible_moves(figure, player):
            if self._is_hop(figure, move, player):
                return True
        return False

def best_move(board, active_player, depth):
    """Generates the best move for a given player.
    Higher depth helps for better quality but exponentially
    raises the computation time."""
    turn_pool = board._move_generator(active_player)

    if depth == 0:
        return random.choice(turn_pool)

    #To keep computation time in reasonable range
    if len(turn_pool) >= 20  and depth >= 3:
        depth = 1
    
    appraisals = []    
    #Appraise every possible move sequence to given depth
    for turn in turn_pool:
        child_board = copy_board(board)
        #If a move is a hop, players do not switch
        if board._is_hop(turn[0], turn[1], active_player):
            player_switch = 1
        else:
            player_switch = -1

        child_board.turn(turn[0], turn[1], active_player)
        appraisals.append(minimax(child_board, active_player * player_switch, depth-1))

    #Choose random move from the list of best appraisals
    #print(all_apps)
    if active_player >= 1:
        return turn_pool[random.choice(maxi(appraisals))]
    elif active_player <= 1:
        return turn_pool[random.choice(mini(appraisals))]
    else:
        raise ValueError(f"Player of weird value: {player} passed to best_move.")

def maxi(list):
    """Returns list index of all maximum values in given list.
    E.g. maxi([0, 10, 0, 10]) would return [1, 3]."""
    return [index for index, value in enumerate(list) if value == max(list)]

def mini(list):
    """Returns list index of all minimum values in given list.
    E.g. mini([0, 10, 0, 10]) would return [0, 2]."""
    return [index for index, value in enumerate(list) if value == min(list)]      

def minimax(board, active_player, depth):
    """Support function for best_move function. Actually does the hard
    work of going through the whole move tree and sending each
    node for appraisal."""
    board_status = board.can_play(active_player, False)
    if board_status is GameState.White_win:
        return 9000
    elif board_status is GameState.Black_win:
        return -9000
    elif board_status is GameState.Draw:
        return 0

    if depth == 0:
        return board._appraise_board()
        
    else:
        minimax_apps = []
        turn_pool = board._move_generator(active_player)

        for turn in turn_pool: #Appraise all children moves
            child_board = copy_board(board)
            #If a move is a hop, players do not switch
            if board._is_hop(turn[0], turn[1], active_player):
                player_switch = 1
            else:
                player_switch = -1

            child_board.turn(turn[0], turn[1], active_player)
            minimax_apps.append(minimax(child_board,
                                        active_player * player_switch,
                                        depth-1))
        
        #Return best appraisal from this subtree
        if board.active_player >= 1:
            return max(minimax_apps)
        elif board.active_player <= -1:
            return min(minimax_apps)
        else:
            raise ValueError(f"Player of weird value: {player} passed to minimax.")
        

def copy_board(origin_board):
    """Deepcopies contents of other Gameboard into a new board."""
    new_board = Gameboard(origin_board.board_size)
    new_board.board = copy.deepcopy(origin_board.board)
    new_board.game_state = origin_board.game_state
    new_board.history = copy.deepcopy(origin_board.history)
    new_board.undocounter = origin_board.undocounter
    new_board.turns_since_hop = origin_board.turns_since_hop
    new_board.active_player = origin_board.active_player
    new_board.player1 = origin_board.player1
    new_board.player2 = origin_board.player2

    return new_board


