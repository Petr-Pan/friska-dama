from logika import *
import pytest

board_size = 10

@pytest.fixture
def board():
    """Provides a Gameboard instance in starting state."""
    return Gameboard(board_size)

def test_move_ok_pos(board):
    assert board.move_ok([6, 1], [5, 0], 1) ==  True
    assert board.move_ok([6, 1], [5, 2], 1) ==  True
    assert board.move_ok([3, 0], [4, 1], -1) ==  True
    assert board.move_ok([3, 2], [4, 1], -1) ==  True

def test_move_ok_neg(board):
    assert board.move_ok([9, 4], [10, 1], 1) ==  False
    assert board.move_ok([0, 0], [0, 10], 1) ==  False
    assert board.move_ok([0, 0], [10, 0], 1) ==  False
    assert board.move_ok([0, 0], [10, 10], 1) ==  False

def test_move_ok_hop_pos(board):
    board.board[5][4] = 1
    board.board[5][2] = -1
    
    assert board.move_ok([5, 4], [5, 0], 1) == True
    assert board.move_ok([5, 2], [5, 6], -1) == True

def test_move_ok_hop_neg(board):
    board.board[5][4] = 1
    board.board[5][2] = -1
    
    assert board.move_ok([6, 1], [5, 0], 1) == False
    assert board.move_ok([3, 0], [4, 1], -1) == False

def test_fields(board):
    assert board.board[0][1] == -1
    assert board.board[0][0] == 'X'
    assert board.board[9][0] == 1
    assert board.board[9][1] == 'X'

def test_check_input(board):
    assert board._check_input([0, 1]) == True
    assert board._check_input([0, 0]) == FieldError.DarkField
    assert board._check_input([10, 0]) == FieldError.NotOnBoard
    assert board._check_input([0, 20]) == FieldError.NotOnBoard
    assert board._check_input([-1, 0]) == True

def test_board_size(board):
    assert len(board.board) == board_size
    assert len(board.board[0]) == board_size

def test_is_players_figure(board):
    assert board.is_players_figure([9,0], 1) == 1
    assert board.is_players_figure([9,1], 1) == FieldError.DarkField
    assert board.is_players_figure([0,1], -1) == 1
    assert board.is_players_figure([0,1], 1) == -1
    assert board.is_players_figure("invalid string", -1) == FieldError.InvalidInput
    assert board.is_players_figure([10,0], 1) == FieldError.NotOnBoard
    assert board.is_players_figure([9,0], 4) == 1

def test_get_field(board):
    assert board.get_field([0,1]) == board.board[0][1]
    assert board.get_field([0,1]) == -1
    assert board.get_field([9,9]) == board.board[9][9]
    assert board.get_field([4,4]) == "X"
    assert board.get_field([8,7]) == board.board[8][7]
    assert board.get_field([8,7]) == 1

def test_turn(board):
    board.turn([6,1], [5,2], 1)
    assert board.board[6][1] == 0
    assert board.board[5][2] == 1

    board.turn([3,8], [4,7], -1)
    assert board.board[3][8] == 0
    assert board.board[4][7] == -1
    assert board.board[5][2] == 1

def test_undo(board):
    #Data preparation
    board.turn([6,1], [5,2], 1)
    board.turn([3,8], [4,7], -1)
    board.turn([3,4], [4,3], -1)
    board.turn([6,5], [5,4], 1)
    assert board.board[3][4] == 0
    assert board.board[6][5] == 0
    assert board.board[5][4] == 1
    assert board.board[4][3] == -1
    #Test
    board.undo()
    assert board.board[5][4] == 0
    assert board.board[4][3] == -1
    assert board.board[3][4] == 0
    assert board.board[6][5] == 1
    board.undo()
    assert board.board[5][4] == 0
    assert board.board[4][3] == 0
    assert board.board[3][4] == -1
    assert board.board[6][5] == 1

def test_redo(board):
    #Data preparation
    board.turn([6,1], [5,2], 1)
    board.turn([3,8], [4,7], -1)
    board.turn([3,4], [4,3], -1)
    board.turn([6,5], [5,4], 1)
    board.undo()
    board.undo()
    #Test
    board.redo()
    assert board.board[5][4] == 0
    assert board.board[4][3] == -1
    assert board.board[3][4] == 0
    assert board.board[6][5] == 1
    board.redo()
    assert board.board[3][4] == 0
    assert board.board[6][5] == 0
    assert board.board[5][4] == 1
    assert board.board[4][3] == -1

def test_check_step(board):
    assert board._check_step([0, 0]) == [0, 0]
    assert board._check_step([9, 2]) == [9, 2]
    assert board._check_step([1, 4]) == [1, 4]
    assert board._check_step([-2, 0]) == [False, False]
    assert board._check_step([10, 0]) == [False, False]
    assert board._check_step([0, 10]) == [False, False]
    assert board._check_step([4, 10]) == [False, False]

def test_get_side(board):
    assert board.get_side(Player.EASY_W) == 1
    assert board.get_side(Player.EASY_B) == -1
    assert board.get_side(Player.MEDIUM_W) == 1
    assert board.get_side(Player.HUMAN_W) == 1
    assert board.get_side(Player.HUMAN_B) == -1
    assert board.get_side(Player.HARD_B) == -1
    assert board.get_side(0) == 0
    assert board.get_side(None) == 0
    assert board.get_side('X') == 0

def test_switchplayers(board):
    #Data preparation
    board.player1 = Player.EASY_W
    board.player2 = Player.EASY_B
    board.active_player = Player.EASY_W
    #Test
    board.switch_players()
    assert board.active_player == Player.EASY_B
    board.switch_players()
    assert board.active_player == Player.EASY_W
    board.active_player = 2
    with pytest.raises(ValueError):
        board.switch_players()

def test_possible_moves_peon(board):
    #Data preparation
    board.turn([6,3], [5,4], 1)
    board.turn([3,0], [4,1], -1)
    board.turn([6,7], [5,8], 1)
    board.turn([2,1], [3,0], -1)
    board.turn([5,4], [4,5], 1)
    
    #Test
    assert board.possible_moves([6,1], 1) == [[5,0],[5,2],[2,1]]
    assert board.possible_moves([3,4], -1) == [[4,3],[5,6]]
    assert board.possible_moves([0,1], -1) == []
    assert board.possible_moves([3,0], -1) == []
    board.turn([4,1], [5,0], -1)
    assert board.possible_moves([5,0], -1) == []
    board.turn([3,8], [4,9], -1)
    assert board.possible_moves([4,9], -1) == [[6,7]]
    assert board.possible_moves([0,7], -1) == []

def test_possible_moves_lady(board):
    #Data preparation
    board._create_empty_board(10)
    board.board[5][4] = 1.5
    board.board[5][2] = -1
    board.board[8][1] = -1
    board.board[0][9] = -1
    board.board[2][7] = 1
    board.board[3][2] = -1
    board.board[2][1] = -1
    #Test
    assert sorted(board.possible_moves([5,4], 1)) == sorted([[5,0],         #Left
                                               [4,3],                       #Up-left
                                               [3,4], [1,4],                #Up
                                               [4,5], [3,6],                #Up-right
                                               [5,6], [5,8],                #Right
                                               [6,5], [7,6], [8,7], [9,8],  #Down-right
                                               [7,4], [9,4],                #Down
                                               [6,3], [7,2], [9,0]])        #Down-left

def test_find_my_figures(board):
    #Data preparation
    board._create_empty_board(10)
    board.board[0][1] = 1
    board.board[7][1] = 1
    board.board[4][7] = 1
    board.board[2][1] = 1
    board.board[9][0] = -1
    #Test
    assert sorted(board._find_my_figures(1)) == sorted([[0,1], [7,1], [4,7], [2,1]])
    assert board._find_my_figures(-1) == [[9,0]]

def test_can_play_no_hops(board):
    #Data preparation
    while board.game_state == GameState.Game_on:
        board.turn([6,1], [5,2], 1)
        board.turn([5,2], [6,1], -1)
        board.can_play(1)
    #Test
    assert board.game_state == GameState.Draw

def test_has_moves(board):
    #Data preparation
    board._create_empty_board(10)
    board.board[1][0] = 1
    board.board[5][2] = 1
    board.board[3][2] = -1
    board.board[4][1] = 1
    board.board[2][1] = 1
    board.board[5][8] = -1
    board.board[3][8] = 1
    #Test
    assert board._has_moves(1) == [[[0,1]], [[1,2],[4,3]], [[2,7],[2,9],[7,8]],
                                    [[3,0],[2,3]], [[4,3],[1,2]]]                               
    assert board._has_moves(-1) == [[[5,0],[4,3],[7,2]], [[6,7],[6,9],[1,8]]]

def test_can_play_endgame(board):
    board._create_empty_board(10)
    board.board[0][9] = -1
    board.board[1][8] = 1
    board.board[3][8] = 1
    board.turn([3,8], [2,7], 1)
    board.switch_players()
    board.can_play(board.active_player)
    assert board.game_state == GameState.White_win

    board._create_empty_board(10)
    board.board[9][0] = 1
    board.board[8][1] = -1
    board.board[6][3] = -1
    board.turn([6,3], [7,2], 1)
    board.switch_players()
    board.can_play(board.active_player)
    assert board.game_state == GameState.Black_win

def test_stone_taking(board):
    #Data preparation
    board._create_empty_board(10)
    board.board[5][4] = -1
    board.board[7][4] = 1
    board.turn([7,4], [3,4], 1)
    #Peon tests
    assert board.board[5][4] == 0
    board.board[2][3] = -1
    board.turn([2,3], [4,5], -1)
    assert board.board[3][4] == 0
    #Queen tests
    board._create_empty_board(10)
    board.board[5][4] = 1.5
    board.board[5][2] = -1
    board.turn([5,4], [5,0], 1)
    assert board.board[5][2] == 0
    #Queen tests 2
    board.board[5][4] = 1.5
    board.board[3][2] = -1
    board.board[2][1] = -1
    board.turn([5,4], [1,0], 1)
    assert board.board[3][2] == 0
    assert board.board[2][1] == 0
    #Queen tests 3
    board.board[9][0] = 1.5
    board.board[7][2] = -1
    board.board[4][5] = -1
    board.board[2][7] = -1
    board.board[1][8] = -1
    board.turn([9,0], [0, 9], 1)
    assert board.board[7][2] == 0
    assert board.board[4][5] == 0
    assert board.board[2][7] == 0
    assert board.board[1][8] == 0
    #Queen tests 4
    board.board[5][4] = 1.5
    board.board[8][7] = -1
    board.turn([5,4], [9,8], 1)
    assert board.board[8][7] == 0

def test_should_become_lady(board):
    board._create_empty_board(10)
    board.board[1][0] = 1
    board.turn([1,0], [0,1], 1)
    assert board.board[0][1] == 1.5

    board.board[8][1] = -1
    board.turn([8,1], [9,2], -1)
    assert board.board[9][2] == -1.5

    board.board[4][3] = 1
    board.board[2][3] = -1
    board.turn([4,3], [0,3], 1)
    assert board.board[0][3] == 1.5

def test_is_hop(board):
    #Data preparation
    board._create_empty_board(10)
    board.board[5][4] = 1
    board.board[4][3] = -1
    board.board[3][4] = -1
    board.board[6][3] = -1
    board.board[8][3] = 1.5
    board.board[9][4] = 1.5
    #Peon tests
    assert board._is_hop([5,4], [3,2], 1) == True
    assert board._is_hop([5,4], [1,4], 1) == True
    assert board._is_hop([5,4], [7,2], 1) == True
    assert board._is_hop([4,3], [6,5], -1) == True
    assert board._is_hop([6,3], [4,5], -1) == True
    assert board._is_hop([3,4], [7,4], -1) == True
    assert board._is_hop([5,4], [4,5], 1) == False
    assert board._is_hop([3,4], [4,5], -1) == False
    #Lady tests
    assert board._is_hop([8,3], [0,3], 1) == True
    assert board._is_hop([9,4], [4,9], 1) == False
