"""Provides a textUI for the game."""

from datetime import datetime
import json
from logika import Player, FieldError, GameState, Gameboard


def start():
    """Provides a basic start menu."""
    print("---------------------------------------------------------------------------------")
    print("Welcome to the game.")
    while True:
        choice = input("Press 'n' for new game. \nPress 'l' to load a previously saved game.\n"
                       "Press 'e' to exit.\n")
        if choice in "Nn":
            newgamemenu()
        elif choice in "Ll":
            loadgamemenu()
        elif choice in "Ee":
            print("Game will now end, bye.")
            break
        elif choice in "Dd":
            #Option for fast manual testing. Change this part for whatever functionality needed
            save = loadgame("Saves\\vert_hop_test.json")
            game(save[0], save[1], save[2])
        else:
            print("Wrong input, sorry. Try again.")

def newgamemenu():
    """Provides a menu for choosing a gamemode."""
    while True:
        choice = input("Press 'c' to play against computer.\n"
                       "Press 'p' to play against another player.\n"
                       "Press 'w' to watch a match between two computer players.\n"
                       "Press 'e' to return to previous menu.\n")
        if choice in "Cc":
            player1, player2 = PvE_menu()
            if player1 and player2:
                game(player1, player2)
            break
        elif choice in "Pp":
            game(Player.HUMAN_W, Player.HUMAN_B)
            break
        elif choice in "Ww":
            player1, player2 = EvE_menu()
            if player1 and player2:
                game(player1, player2)
            break
        elif choice in "Ee":
            break
        else:
            print("Wrong input, sorry. Try again.")
    return 0

def PvE_menu():
    """Provides a menu for choosing AI opponent in PvE game."""
    msg = ("Enter:\n'1' for easy opponent\n" \
           "'2' for medium opponent\n" \
           "'3' for hard opponent\n" \
           "'abort' to abort.\n")

    side_msg = ("Choose your side:\n" \
                "Enter 'white' for white figures or " \
                "enter 'black' for black figures. " \
                "Enter 'abort' to abort.\n")
    
    while True: #Choosing player side
        side = input(side_msg)
        if side in ("abort", "Abort"):
            return False, False
        side = _validateside(side)
        if side is False:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue
        player1, _ = _assignPlayer('4', side)
        break

    while True: #Choosing opponent
        print("Setting opponent:")
        print("Choose type of player:")
        inputted = input(msg)
        if inputted in ("abort", "Abort"):
            return False, False
        PvEside = 'black' if player1 > 1 else 'white'
        player2, correct_value = _assignPlayer(inputted, PvEside)
        if correct_value:
            break
        else:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue
    
    if player1 < player2: #So that player1 is white
        player1, player2 = player2, player1

    return player1, player2

def EvE_menu():
    """Provides a menu for choosing AI players in a spectator game."""
    msg = ("Enter:\n'1' for easy AI\n" \
           "'2' for medium AI\n" \
           "'3' for hard AI\n" \
           "'abort' to abort.\n")

    while True: #Choosing first AI
        print("Setting player1 (white figures):")
        inputted = input(msg)
        if inputted in ("abort", "Abort"):
            return False, False
        side = 'white'
        player1, correct_value = _assignPlayer(inputted, side)
        if correct_value:
            break
        else:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue

    while True: #Choosing second AI
        print("Setting player2 (black figures):")
        inputted = input(msg)
        if inputted in ("abort", "Abort"):
            return False, False
        side = 'black'
        player2, correct_value = _assignPlayer(inputted, side)
        if correct_value:
            break
        else:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue

    return player1, player2

def loadgamemenu():
    """Provides a menu for loading json saved games."""
    while True:
        inputted = input("Write the name of the file to load. "
                         "Press 'e' to return to previous menu.\n")
        save = loadgame(inputted)
        if save:
            break
        print("Wrong name or incorrect format.")
    game(save[0], save[1], save[2])

def loadgame(save):
    """Deals with possible error states during file loading and extracts game data from file."""
    try:
        with open(save, "r") as file:
            history = json.load(file)
            last_turn = history[-1]
            penult_turn = history[-2]
            player1 = Player(last_turn[2])
            player2 = Player(penult_turn[2])
            if player1 < player2:
                player1, player2 = player2, player1
    except FileNotFoundError:
        print("File not found.")
        return False
    except IndexError:
        print("File not valid.")
        return False
    else:
        return player1, player2, history

def game_end_print(board):
    """Prints the result of the game to the TLI"""
    if board.game_state == GameState.White_win:
        print("Congrats to the white player.")
    elif board.game_state == GameState.Black_win:
        print("Congrats to the black player.")
    elif board.game_state == GameState.Draw:
        print("Wow, a draw!")
    else:
        print("Game ended prematurely, see ya.")

def game(player1, player2, load_game=False):
    """Governs the game process. Takes types of players as its arguments.
       Players 1 and -1 indicate a human player. Players 2 and -2 indicate a computer player.
       In the future, 3 and -3, 4 and -4 etc. might indicate higher difficulties."""
    board = Gameboard(10)
    board.active_player = player1 if player1 > player2 else player2 #So that white player starts
    board.player1 = player1
    board.player2 = player2
    if load_game:
        board.load_history(load_game)
        board.history = load_game
        #Active player is the one that played in penult turn (= load_game[-2])
        #Player is third item in turn list
        board.active_player = Player(load_game[-2][2])
    board.print()

    while board.game_state == GameState.Game_on:
        if board.game_state != GameState.Game_on:
            break
        print("Player on the turn:", get_side_for_humans(board.active_player))
        if board.active_player in (Player.HUMAN_B, Player.HUMAN_W):
            while True:
                figure = getfiguremenu(board, board.active_player, board.player1, board.player2)
                if board.game_state != GameState.Game_on or figure == "changing players":
                    break
                move = getmovemenu(board, board.active_player, figure)
                if move != "change figure":
                    break
        else:
            c_turn = board.computer_turn(board.active_player)
            figure = c_turn[0]
            move = c_turn[1]
        if board.game_state != GameState.Game_on:
            break
        if figure == "changing players":
            continue

        if not board._is_hop(figure, move, board.active_player):
            board.turn(figure, move, board.active_player)
            board.switch_players()
        else:
            board.turn(figure, move, board.active_player)
            
        if board.has_hopped and not board.can_hop(move, board.active_player):
            board.switch_players()
            
        board.print()
        

    game_end_print(board)



def getfiguremenu(board, active_player, player_1, player_2):
    """Gets user input for selecting figure and provides an in-game menu."""
    msg = "Give row and column number of figure that you want to play."
    inputted = input(msg + "\nType 'help' for list of other available commands.\n")

    while True:
        if inputted == "switch":
            board.switch_players()
            board.print()
            print("Player on the turn:", board.active_player)
            if board.active_player not in (Player.HUMAN_B, Player.HUMAN_W):
                return "changing players"
        elif inputted in "EndendExitexit" and inputted != "":
            board.game_state = GameState.Exit
            return False
        elif inputted == "help":
            print("List of available commands:")
            print("hint - recommends a move")
            print("switch - switches active player")
            print("end - ends the game")
            print("print - prints the game board again")
            print("savegame - saves current game into file")
            print("change players - brings menu for changing players")
        elif inputted == "hint":
            print("Pondering about strategy...")
            best_move = board.computer_turn_hard(board.active_player)
            print(f"I'd move figure {best_move[0]} to field {best_move[1]}.")
        elif inputted == "print":
            board.print()
        #Debugging option
        elif inputted == "player":
            print(board.active_player)
            print(f"player1: {board.player1}")
            print(f"player2: {board.player2}")
        elif inputted == "savegame":
            if len(board.history) >= 2:
                savegamemenu(board)
            else:
                print("Cannot save game with less than two turns.")
        elif inputted == "undo":
            board.undo()
            board.print()
        elif inputted == "redo":
            board.redo()
            board.print()
        elif inputted == "history":
            for turn in board.history:
                print(turn)
        elif inputted == "uc":
            print(board.undocounter)
        elif inputted == "change players":
            changeplayersmenu(board)
            return "changing players"
        else:
            try:
                [int(x) for x in inputted.split(" ")]
            except ValueError:
                print("The input you have entered is invalid. Please, try again.")
                print("Type 'help' for list of other available commands.")
            else:
                inputted = [int(x) for x in inputted.split(" ")]
                figure_valid = board.is_players_figure(inputted, board.active_player)
                if figure_valid == -1:
                    print("That is an opponent's figure. Try again.")
                elif figure_valid == FieldError.NotOnBoard:
                    print("This field is not on the board. Please try again.")
                elif figure_valid == FieldError.DarkField:
                    print("This is a non-playable dark field. Try again.")
                elif figure_valid == FieldError.EmptyField:
                    print("This is an empty field. Try again.")
                if figure_valid == 1:
                    break

        inputted = input(msg + "\n")
    return inputted

def getmovemenu(board, active_player, figure):
    """Gets user input for move target and provides an in-game menu."""
    msg = "Give row and column number of field where you want to place the figure."
    inputted = input(msg + "\nType 'help' for list of other available commands.\n")

    while True:
        if inputted in "EndendExitexit" and inputted != "":
            board.game_state = GameState.Exit
            return False
        elif inputted == "help":
            print("List of available commands:")
            print("end - ends the game")
            print("print - prints the game board again")
            print("change figure - let's you choose figure again")
        elif inputted == "print":
            board.print()
        elif inputted == "savegame":
            if len(board.history) >= 2:
                savegamemenu(board)
            else:
                print("Cannot save game with less than two turns.")
        elif inputted == "moves":
            print(board.possible_moves(figure, active_player))
        elif inputted == "change figure":
            return inputted
        else:
            try:
                [int(x) for x in inputted.split(" ")][1]
            except (ValueError, IndexError):
                print("The input you have entered is invalid. Please, try again.")
                print("Type 'help' for list of other available commands.")
            else:
                inputted = [int(x) for x in inputted.split(" ")]
                if board.move_ok(figure, inputted, active_player):
                    break
                else:
                    print("This move is invalid.")
                if board.get_field([inputted[0], inputted[1]]) == "X":
                    print("This is not a playable field, try again.")
        inputted = input(msg + "\n")
    return inputted

def savegamemenu(board):
    """Provides menu to save current state of the board into file."""
    curr_timestamp = str(datetime.now())
    msg = ("Please write name of the save game. If you leave this field empty,"
           "current date and time will be used instead: " + curr_timestamp + "\n"
           "Enter 'abort' to return to previous menu.\n")
    inputted = input(msg)

    if inputted == "":
        inputted = curr_timestamp
    if inputted in ("abort", "Abort"):
        return 1

    while True:
        if inputted in ("abort", "Abort"):
            break
        save_result = board.save_game('Saves//' + inputted + '.json')
        if save_result[0] == 0:
            print("File saved successfully as: '", inputted, "'")
            break
        elif save_result[0] == FileExistsError:
            print("File already exists.")
        elif save_result[0] == OSError:
            print("A system error has ocurred.")
            print(save_result[1])
        print("Save was unsuccessful. If you wish to abort saving, type 'abort'.")
        inputted = input(msg)

    return 0

def changeplayersmenu(board):
    """Provides a menu for changing players during a game."""
    msg = ("Enter:\n'1' for easy opponent\n" \
           "'2' for medium opponent\n" \
           "'3' for hard opponent\n" \
           "'4' for human player\n" \
           "'abort' to abort.\n")

    side_msg = ("Select side:\n" \
                "Enter 'white' for white figures or " \
                "enter 'black' for black figures. " \
                "Enter 'abort' to abort.\n")

    if board.active_player == board.player1: #Saving actual active player
        active_player_number = 1
    else:
        active_player_number = 2
    
    while True: #Choosing new player1
        print("Setting player1:")
        side = input(side_msg)
        if side in ("abort", "Abort"):
            return
        
        side = _validateside(side)
        if side is False:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue

        print("Choose type of player:\n")
        inputted = input(msg)
        if inputted in ("abort", "Abort"):
            return

        possible_player1, correct_value = _assignPlayer(inputted, side)
        if correct_value:
            board.player1 = possible_player1
            break
        else:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue

    while True: #Choosing new player2
        print("Setting player2:")
        side = input(side_msg)
        if side in ("abort", "Abort"):
            return
        
        side = _validateside(side)
        if side is False:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue

        print("Choose type of player:\n")
        inputted = input(msg)
        if inputted in ("abort", "Abort"):
            return

        possible_player2, correct_value = _assignPlayer(inputted, side)
        if correct_value:
            board.player2 = possible_player2
            break
        else:
            print("Provided input was incorrect. Please try again or abort.\n")
            continue
    
    if active_player_number == 1:
        board.active_player = board.player1
    else:
        board.active_player = board.player2
    
    return


def _assignPlayer(input, side):
    """Checks player input for numbers from 1 to 4 and
    transforms them into appropriate Player enum."""
    player_int = _assignPlayer_check(input, side)
    if player_int:
        PLAYER_CONVERT_DICT = {1: Player.EASY_W, -1: Player.EASY_B,
                    2: Player.MEDIUM_W, -2: Player.MEDIUM_B,
                    3: Player.HARD_W, -3: Player.HARD_B,
                    4: Player.HUMAN_W, -4: Player.HUMAN_B}
        return PLAYER_CONVERT_DICT[player_int], True
    else:
        return False, False


def _assignPlayer_check(input, side):
    """Checks that input is a number from 1 to 4. and
    converts into black figures player depending on
    side argument."""
    try:
        player_int = int(input)
    except ValueError:
        return False
    if player_int in (1,2,3,4):
        if side == 'black':
            player_int = player_int * -1
        return player_int
    else:
        return False

def _validateside(input):
    """Checks that input is a string that can be processed
    in _assignPlayer_check function."""
    if input == "black":
        return input
    elif input == "white":
        return input
    else:
        return False

def get_side_for_humans(player):
    """Returns side of inputted player. Player with positive figures is player 1,
       player with negative figures is player 2."""
    if player >= 1:
        return "White"
    elif player <= -1:
        return "Black"
    else:
        return 0
