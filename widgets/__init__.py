﻿"""Contains GUI classes for the game."""

from widgets.board_window import BoardWindow
from widgets.players_dialog import PlayersDialog
from widgets.universal_dialog import UniversalDialog
from widgets.ai_worker import AIWorker, AIWorkerSignals