import PyQt5.QtCore as qc

import traceback, sys, random
from logika import minimax, maxi, mini, Player, copy_board

class AIWorker(qc.QRunnable):
    """Asynchronously computes AI turn of a given gameboard."""

    def __init__(self, board, main_window):
        super().__init__()

        self.signals = AIWorkerSignals()    
        self._is_running = True
        self._abort = False
        self.board = board
        self.depth = self._calculate_depth()
        self.main_window = main_window

    @qc.pyqtSlot()
    def run(self):
        """Calls passed function with supplied args/kwargs.
        Sends any exception that was encountered as an error signal.
        Result of passed function is sent as a signal.
        Emits 'finished' signal upon completion/exception."""

        try:
            result = self.best_move_async(self.board, self.depth)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.finished.emit()

    @qc.pyqtSlot()
    def best_move_async(self, board, depth):     
        """Generates the best move for a given player.
        Higher depth helps for better quality but exponentially
        raises the computation time. Emits progress signal as root paths
        of best move algorithm are completed."""
        active_player = board.active_player
        turn_pool = board._move_generator(board.active_player)

        if depth == 0:
            return random.choice(turn_pool)

        #To keep computation time in reasonable range
        if len(turn_pool) >= 20  and depth >= 3:
            depth = 1
    
        appraisals = []    
        progress = 0
        #Appraise every possible move sequence to given depth
        for turn in turn_pool:
            if self._abort:
                return False

            if not self._is_running == True:
                break
            
            self.signals.progress.emit(progress*100//len(turn_pool))

            child_board = copy_board(board)
            #If a move is a hop, players do not switch
            if board._is_hop(turn[0], turn[1], active_player):
                player_switch = 1
            else:
                player_switch = -1

            child_board.turn(turn[0], turn[1], active_player)
            appraisals.append(minimax(child_board, active_player * player_switch, depth-1))

            progress += 1                     

        if not appraisals: # Thread has been closed too prematurely
            return random.choice(turn_pool)
            
        self.signals.progress.emit(progress*100//len(turn_pool))
        
        #Choose random move from the list of best appraisals
        if active_player >= 1:
            return turn_pool[random.choice(maxi(appraisals))]
        elif active_player <= 1:
            return turn_pool[random.choice(mini(appraisals))]
        else:
            raise ValueError(f"Player of weird value: {player} passed to best_move.")

    def stop(self):
        """Stops the computation prematurely. A move that has been deemed
        the best so far is sent."""
        self._is_running = False
        self.main_window.AI_progress_bar.setValue(0)

    def abort(self):
        """Stops the computation prematurely and causes best_move_async
        to not send a valid result."""
        self._abort = True
        self.main_window.AI_progress_bar.setValue(0)

    def _calculate_depth(self):
        """Calculates best move depth for active player of gameboard
        assigned to the worker."""
        if self.board.active_player in (Player.EASY_W, Player.EASY_B):
            return 0
        elif self.board.active_player in (Player.MEDIUM_W, Player.MEDIUM_B):
            return 1
        elif self.board.active_player in (Player.HARD_W, Player.HARD_B):
            return 3
        elif self.board.active_player in (Player.HUMAN_W, Player.HUMAN_B):
            return 3


class AIWorkerSignals(qc.QObject):
    """Defines signals for Worker class."""

    finished = qc.pyqtSignal()
    error = qc.pyqtSignal(tuple)
    result = qc.pyqtSignal(object)
    progress = qc.pyqtSignal(int)

