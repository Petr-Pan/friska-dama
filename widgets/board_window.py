﻿import PyQt5.QtCore as qc
import PyQt5.QtGui as qg
import PyQt5.QtWidgets as qw
from PyQt5 import uic

from logika import Player, FieldError, GameState, Gameboard
from textUI import get_side_for_humans, _assignPlayer
from widgets.universal_dialog import UniversalDialog
from widgets.ai_worker import AIWorker
from widgets.config.colours import Colours

class BoardWindow(qw.QOpenGLWidget):
    """Widget displaying the gameboard and providing its manipulation."""
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.gameboard = Gameboard(10)
        self.setMinimumSize(400, 300)
        self.setSizePolicy(qw.QSizePolicy.Expanding, qw.QSizePolicy.Expanding)
        self.selected_figure = []
        self.hinted_turn = None
        self.AI_worker = None

        self.AI_timer = qc.QTimer(self)
        self.AI_timer.start(500)
        self.AI_timer.timeout.connect(self.AI_turn)
               
    def compute_hint(self):
        """Computes best move for highest difficulty for active player."""
        self.AI_worker = AIWorker(self.gameboard, self.parent)
        self.AI_worker.signals.result.connect(self.set_hint)
        self.AI_worker.signals.progress.connect(self.track_progress)
        self.parent.stop_computing_butt.pressed.connect(self.AI_worker.stop)

        self.parent.thread_pool.start(self.AI_worker)

    def set_hint(self, hint):
        """Takes a computed AI turn and sets it to hint slot."""
        self.AI_worker = None

        if hint == False:
            return

        self.hinted_turn = hint
        self.update()
        
    def AI_turn(self):
        """Governs AI turn handling."""        
        if (self.gameboard.active_player not in (Player.HUMAN_W, Player.HUMAN_B)
                and self.gameboard.game_state == GameState.Game_on):
            if self.gameboard.game_state != GameState.Game_on:
                return

            self.AI_worker = AIWorker(self.gameboard, self.parent)
            self.AI_worker.signals.result.connect(self._do_AI_turn)
            self.AI_worker.signals.progress.connect(self.track_progress)
            self.parent.stop_computing_butt.pressed.connect(self.AI_worker.stop)

            self.parent.thread_pool.start(self.AI_worker)
            self.AI_timer.stop()  
            
    def _do_AI_turn(self, turn):
        """Takes a computed AI turn and completes it."""
        self.AI_worker = None

        if turn == False:
            return

        figure = turn[0]
        move = turn[1]

        if not self.gameboard._is_hop(figure, move, self.gameboard.active_player):
            self.turn(figure, move, self.gameboard.active_player)
            self.switch_players()
        else:
            self.turn(figure, move, self.gameboard.active_player)
                
        self.update()
        self.AI_timer.start()

    def track_progress(self, progress):
        """Simple report for tracking AIWorker progress of computation of best move."""
        self.parent.AI_progress_bar.setValue(progress)
        if progress == 100:
            self.parent.AI_progress_bar.reset()

    def AI_start(self):
        """Starts the AI timer."""
        self.AI_timer.start()

    def AI_stop(self):
        """Stops the AI timer."""
        self.AI_timer.stop()

    def turn(self, figure, move, player):
        """Makes a turn on the gameboard and services appropriate
        GUI elements - history_view table."""
        self.gameboard.turn(figure, move, player)
        self.hinted_turn = None
        self.parent.history_update()
        if self.gameboard.has_hopped and not self.gameboard.can_hop(move, self.gameboard.active_player):
            self.switch_players()

    def switch_players(self):
        """Switches active player of the board."""
        self.gameboard.switch_players()
        self.parent.active_player_update()
        self.game_state_check()
        self.update()

    def game_state_check(self):
        """Regular check of game state:
                if active player can still play (i.e. he is not blocked).
                if player hopped and cannot hop furthermore."""
        if self.gameboard.game_state != GameState.Game_on:            
            self.endgame()
            
    def endgame(self):
        """Displays dialog about the result of the game."""
        if self.gameboard.game_state == GameState.Draw:
            title = "Draw"
            text = "The match has ended in a not so glorious draw."
        elif self.gameboard.game_state == GameState.White_win:
            title = "White player won."
            text = "Congratulations to the white player!"
        elif self.gameboard.game_state == GameState.Black_win:
            title = "Black player won."
            text = "Congratulations to the black player!"

        UniversalDialog(self.parent, title, text)
        
    def mousePressEvent(self, event):
        """Handles mousePressEvent for the BoardWindow.
        Takes care of selecting appropriate figures and their targeted moves."""

        target = self.pixels_to_indexes(event.x(), event.y())
        target = list(target)
        
        if (self.gameboard.is_players_figure(target, self.gameboard.active_player) == 1
                and self.gameboard.active_player in (Player.HUMAN_W, Player.HUMAN_B)):
            if target != self.selected_figure:
                self._select_figure(target)
            else:
                self._select_figure([])

        elif self.selected_figure:
            move = target
            if self.gameboard.move_ok(self.selected_figure, move, self.gameboard.active_player):            
                if not self.gameboard._is_hop(self.selected_figure, move, self.gameboard.active_player):
                    self.turn(self.selected_figure, move, self.gameboard.active_player)
                    self.switch_players()
                else:
                    self.turn(self.selected_figure, move, self.gameboard.active_player)

                if (self.gameboard.player1 not in (Player.HUMAN_W, Player.HUMAN_B)
                    or self.gameboard.player2 not in (Player.HUMAN_W, Player.HUMAN_B)):
                    if not self.AI_timer.isActive(): # If AI was turned off for whatever reason, player turn should turn it on again
                        self.parent._AI_turn_on()
                    elif self.AI_worker: # Hint computation could have been in progress, but if the player has played, he does not need the hint anymore
                        self.AI_worker.abort()
            
            if self.gameboard.has_hopped: # Highlight multihopping figure
                if self.selected_figure != self.gameboard.has_hopped:
                    self._select_figure(self.gameboard.has_hopped)                    
            else:
                self._select_figure([])
       
        else:
            pass

    def paintEvent(self, event):
        """Repaints the board if a paintEvent is sent."""
        painter = qg.QPainter()
        painter.begin(self)

        field_width, field_height = self.field_size()
        board_size = self.gameboard.board_size

        self.paint_board(painter, field_width, field_height, board_size)
        self.paint_figures(painter, field_width, field_height, board_size)

        painter.end()

    def paint_board(self, painter, field_width, field_height, board_size):
        """Paints the board background."""
        #Draw field outline
        painter.setPen(Colours.field_outline)
        for row in range(board_size):
            for col in range(board_size):
                x, y = self.indexes_to_pixels(row, col)
                rect = qc.QRect(x, y, field_width, field_height)
                
                painter.setPen(Colours.field_outline)
                painter.drawRect(rect)
  
        #Draw fields
        for row in range(board_size):
            for col in range(board_size):
                x, y = self.indexes_to_pixels(row, col)
                rect = qc.QRect(x, y, field_width, field_height)

                if self.gameboard.get_field([row, col]) == 'X':
                    colour = Colours.dark_field 
                elif [row, col] == self.selected_figure:
                    colour = Colours.selected_figure_background
                else:
                    colour = Colours.light_field

                if self.hinted_turn: # Highlight hinted figure and its move
                    if self.hinted_turn[0] == self.selected_figure and self.hinted_turn[0] == [row, col]:
                        colour = Colours.selected_hint_figure_background
                    elif self.hinted_turn[0] == [row, col]:
                        colour = Colours.hint_figure_background
                    elif self.hinted_turn[1] == [row, col]:
                        colour = Colours.hint_move_background
                
                painter.setBrush(qg.QBrush(colour))
                painter.drawRect(rect)

    def paint_figures(self, painter, field_width, field_height, board_size):
        """Paints the board figures."""

        for row in range(board_size):
            for col in range(board_size):
                x, y = self.indexes_to_pixels(row, col)
                white = Colours.white_figure
                black = Colours.black_figure
                figure = self.gameboard.get_field([row, col])
                
                if figure == 1:
                    self._paint_peon(painter, x, y, field_width, field_height, white)

                elif figure == -1:
                    self._paint_peon(painter, x, y, field_width, field_height, black)

                elif figure == 1.5:
                    self._paint_lady(painter, x, y, field_width, field_height, white)

                elif figure == -1.5:
                    self._paint_lady(painter, x, y, field_width, field_height, black)

                else:
                    pass

    def _paint_peon(self, painter, x, y, field_width, field_height, color):
        """Paints a peon at x, y."""
        width = field_width / 2
        height = field_height / 2
        x += width - width / 2 
        y += height - height / 2

        rect = qc.QRect(x, y, width, height)

        painter.setPen(color)
        painter.setBrush(qg.QBrush(color))
        painter.drawEllipse(rect)

    def _paint_lady(self, painter, x, y, field_width, field_height, color):
        """Paints a lady at x, y."""
        width = field_width / 2
        height = field_height / 1.7
        x += width - width / 2 
        y += height - height / 1.6

        top = qg.QPolygonF()
        top.append(qc.QPointF(x+width, y))
        top.append(qc.QPointF(x+width/2, y+height/1.5))
        top.append(qc.QPointF(x, y))

        skirt = qg.QPolygonF()
        skirt.append(qc.QPointF(x, y+height))
        skirt.append(qc.QPointF(x+width, y+height))
        skirt.append(qc.QPointF(x+width/2, y+height/2.5))

        painter.setPen(color)
        painter.setBrush(qg.QBrush(color))
        painter.drawPolygon(top)
        painter.drawPolygon(skirt)

    def field_size(self):
        """Returns pixel size of a field of the game board."""
        size = self.size()
        width = size.width()
        height = size.height()
        return width // self.gameboard.board_size, height // self.gameboard.board_size, 

    def indexes_to_pixels(self, row, col):
        """Converts indexes of Gameboard.board to pixels of BoardWindow."""
        field_width, field_height = self.field_size()
        return col * field_width, row * field_height 

    def pixels_to_indexes(self, x, y):
        """Converts pixels of BoardWindow to indexes of Gameboard.board."""
        field_width, field_height = self.field_size()
        return y // field_height, x // field_width

    def _select_figure(self, figure=[]):
        """Sets clicked figure as selected_figure and updates the GUI."""
        self.selected_figure = figure
        self.update()
