import PyQt5.QtGui as qg

class Colours:
    """Contains colours in pyqt compatible format for the GUI module
    of friska dama."""
    white_figure = qg.QColor(210, 210, 210)
    black_figure = qg.QColor(0, 0, 0)
    field_outline = qg.QColor(100, 100, 100)
    dark_field = qg.QColor(53, 15, 1)
    light_field = qg.QColor(200, 150, 130)
    selected_figure_background = qg.QColor(150, 100, 70)
    selected_hint_figure_background = qg.QColor(30, 120, 20)
    hint_figure_background = qg.QColor(50, 150, 40)
    hint_move_background = qg.QColor(50, 175, 40)
   
