class CSS_Stylesheet_Edit:
    """Contains short css edits in pyqt compatible format for the GUI module
    of friska dama."""
    AI_butt_on = "background-color: rgb(50, 200, 50)"
    AI_butt_off = "background-color: rgb(200, 50, 50)"
    AI_butt_disabled = "background-color: rgb(200, 200, 200)"
       
