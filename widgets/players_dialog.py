﻿import PyQt5.QtCore as qc
import PyQt5.QtGui as qg
import PyQt5.QtWidgets as qw
from PyQt5 import uic

from logika import Player, Gameboard
from textUI import _assignPlayer


class PlayersDialog(qw.QDialog):
    """Dialog for setting players. Used in new game, change players and
    change difficulty QActions."""
    def __init__(self, window, board_window, is_new_game):
        super().__init__(window)

        with open('.\\templates\\players_dialog.ui') as template:
            uic.loadUi(template, self)

        self.main_window = window
        self.board_window = board_window

        self.is_new_game = is_new_game

        self.white_buttg = self.findChild(qw.QButtonGroup,'white_buttg')
        self.black_buttg = self.findChild(qw.QButtonGroup,'black_buttg')
        
        self.OK_butt = self.findChild(qw.QPushButton,'')

        self.OK_butt.pressed.connect(self.accepted)

        self.show()

    def accepted(self):
        """Gets called upon OK click. Sets the players selected
        via radio buttons to binded gameboard."""
        checked_white = self.white_buttg.checkedButton().text()
        checked_black = self.black_buttg.checkedButton().text()

        if self.is_new_game:
            self.board_window.gameboard = Gameboard(10)
            self.board_window.hinted_turn = None
            self.main_window.active_player_update()
            self.main_window.history_update()

        player1 = self._butt_text_to_player(checked_white, "white")
        player2 = self._butt_text_to_player(checked_black, "black")

        if self.board_window.gameboard.active_player >= 1:
            self.board_window.gameboard.active_player = player1
        else:
            self.board_window.gameboard.active_player = player2

        self.board_window.gameboard.player1 = player1
        self.board_window.gameboard.player2 = player2

        self.main_window.AI_setup()
        
        self.close()

    def _butt_text_to_player(self, button_text, side):
        """Assigns appropriate Player instance based on button text of 
        PlayersDialog template."""
        if "Easy" in button_text:
            return _assignPlayer(1, side)[0]
        elif "Medium" in button_text:
            return _assignPlayer(2, side)[0]
        elif "Hard" in button_text:
            return _assignPlayer(3, side)[0]
        elif "Human" in button_text:
            return _assignPlayer(4, side)[0]