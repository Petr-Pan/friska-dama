﻿import PyQt5.QtCore as qc
import PyQt5.QtGui as qg
import PyQt5.QtWidgets as qw
from PyQt5 import uic


class UniversalDialog(qw.QDialog):
    """Base class for various simple QDialogs. They take a parent window,
    title and main text as an input parameter."""
    def __init__ (self, window, title, text, width=None, height=None):
        super().__init__(window)
        
        with open('.\\templates\\universal_dialog.ui') as template:
            uic.loadUi(template, self)

        self.setWindowTitle(title)
        
        self.text = self.findChild(qw.QTextEdit, 'text_area')
        self.text.setText(text)

        if width and height:
            self.resize(width, height)
        else:
            self._adjust_height(text)
        
        self.show()

    def _adjust_height(self, text):
        """Adjusts height of UniversalDialog for the amount of text given."""
        newline_chars = text.count('\n')
      
        if newline_chars:
            self.setMinimumHeight(self.height() + newline_chars * 20)